#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent), ui(new Ui::MainWindow),
		trAdd(new TriangleAdd()), parallAdd(new ParallelogramAdd()),
		context(new AddDrawContext(&trAdd))
{
	ui->setupUi(this);

	repaint();
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::checkCheckBox()
{
	if (ui->checkBox->checkState() == Qt::CheckState::Checked)
		repaint();
}

void MainWindow::paintEvent(QPaintEvent* e)
{
	Q_UNUSED(e);

	IntPtr<QPainter> painter(new QPainter(this));

	painter->setPen(QPen(Qt::black, 2, Qt::SolidLine, Qt::FlatCap));

	Vector2 v1(ui->v1_ax_edit->text().toInt() % width + xOffset,
						 ui->v1_ay_edit->text().toInt() % height + yOffset,
						 ui->v1_bx_edit->text().toInt() % width + xOffset,
						 ui->v1_by_edit->text().toInt() % height + yOffset);
	PaintHelper::drawVector(v1, "v1", &painter);

	Vector2 v2(ui->v2_ax_edit->text().toInt() % width + xOffset,
						 ui->v2_ay_edit->text().toInt() % height + yOffset,
						 ui->v2_bx_edit->text().toInt() % width + xOffset,
						 ui->v2_by_edit->text().toInt() % height + yOffset);
	PaintHelper::drawVector(v2, "v2", &painter);

	context->addVectors(v1, v2, &painter);
}

void MainWindow::on_help_triggered()
{
		QMessageBox::about(this, "Справка",
			"Добро пожаловать в программу `VectorEditor`, который может складывать вектора двумя способами:"
			"методом треугольника и параллелограмма. Для изменений в реальном времени отметьте соответствующий checkBox.");
}

void MainWindow::on_author_triggered()
{
		QMessageBox::about(this, "Об авторе", "Автором программы является:\n\tЧерных Артём.");
}

void MainWindow::on_comboBox_currentIndexChanged(int index)
{
		if(index == 0)
			context->setAddDrawStrategy(&trAdd);
		else
			context->setAddDrawStrategy(&parallAdd);

		repaint();
}

void MainWindow::on_pushButton_clicked() { repaint(); }

void MainWindow::on_v1_ax_edit_textEdited() { checkCheckBox(); }
void MainWindow::on_v1_ay_edit_textEdited() { checkCheckBox(); }
void MainWindow::on_v1_bx_edit_textEdited() { checkCheckBox(); }
void MainWindow::on_v1_by_edit_textEdited() { checkCheckBox(); }

void MainWindow::on_v2_ax_edit_textEdited() { checkCheckBox(); }
void MainWindow::on_v2_ay_edit_textEdited() { checkCheckBox(); }
void MainWindow::on_v2_bx_edit_textEdited() { checkCheckBox(); }
void MainWindow::on_v2_by_edit_textEdited() { checkCheckBox(); }
