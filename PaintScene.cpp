#include "PaintScene.h"

void PaintScene::mousePressEvent(QGraphicsSceneMouseEvent* e)
{
	if(prevState == 0)
	{
		prevPoint = e->scenePos();
		prevState = 1;
	}
	else
	{
		addLine(prevPoint.x(),
						prevPoint.y(),
						e->scenePos().x(),
						e->scenePos().y(),
						QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap));
		prevPoint = e->scenePos();

		prevState = 0;
	}
}

/*void PaintScene::mouseMoveEvent(QGraphicsSceneMouseEvent *e)
{
	addLine(prevPoint.x(),
					prevPoint.y(),
					e->scenePos().x(),
					e->scenePos().y(),
					QPen(Qt::black, 10, Qt::SolidLine, Qt::FlatCap));
	prevPoint = e->scenePos();
}*/
