#ifndef PARALLELOGRAMADD_H
#define PARALLELOGRAMADD_H

#include "IAddDrawStrategy.h"

class ParallelogramAdd : public IAddDrawStrategy
{
public:
	virtual Vector2& add(const Vector2 &v1, const Vector2 &v2, QPainter *painter) override
	{
		// Переносим 2 вектор к началу 1-го.
		IntPtr<Vector2> temp1(new Vector2(v1.ax, v1.ay, v1.ax + v2.bx - v2.ax, v1.ay + v2.by - v2.ay));
		PaintHelper::drawVector(*temp1, "temp1 (v2)", painter);

		// Переносим 2 вектор к концу 1-го.
		IntPtr<Vector2> temp2(new Vector2(v1.bx, v1.by, v1.bx + v2.bx - v2.ax, v1.by + v2.by - v2.ay));
		PaintHelper::drawVector(*temp2, "temp2 (v2)", painter);

		// Переносим 1 вектор к концу 2-го.
		IntPtr<Vector2> temp3(new Vector2(temp1->bx, temp1->by, temp1->bx + v1.bx - v1.ax, temp1->by + v1.by - v1.ay));
		PaintHelper::drawVector(*temp3, "temp3 (v1)", painter);

		// Получаем результат сложения векторов.
		IntPtr<Vector2> res(new Vector2(v1.ax, v1.ay, temp3->bx, temp3->by));
		QPen p = painter->pen();
		painter->setPen(QPen(Qt::red, 3, Qt::SolidLine, Qt::FlatCap));
		PaintHelper::drawVector(*res, "res", painter);
		painter->setPen(p);

		return *res;
	}
};

#endif // PARALLELOGRAMADD_H
