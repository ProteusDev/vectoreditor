#ifndef VECTOR2_H
#define VECTOR2_H

class Vector2
{
public:
	int ax, ay, bx, by;
	Vector2() : ax(0), ay(0), bx(0), by(0) { }
	Vector2(int _ax, int _ay, int _bx, int _by) : ax(_ax), ay(_ay), bx(_bx), by(_by) { }
	Vector2(const Vector2& v) : ax(v.ax), ay(v.ay), bx(v.bx), by(v.by) { }
};

#endif // VECTOR2_H
