#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>

#include "IntPtr.hpp"
#include "Vector2.hpp"
#include "IAddDrawStrategy.h"
#include "TriangleAdd.hpp"
#include "ParallelogramAdd.hpp"
#include "AddDrawContext.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

private:
	const int width = 300, height = 250, xOffset = 50, yOffset = 80;

	Ui::MainWindow *ui;

	IntPtr<IAddDrawStrategy> trAdd;
	IntPtr<IAddDrawStrategy> parallAdd;
	IntPtr<AddDrawContext> context;

	void checkCheckBox();

protected:
	void paintEvent(QPaintEvent* e);

private slots:
	void on_help_triggered();
	void on_author_triggered();
	void on_comboBox_currentIndexChanged(int index);
	void on_pushButton_clicked();
	void on_v1_ax_edit_textEdited();
	void on_v1_ay_edit_textEdited();
	void on_v1_bx_edit_textEdited();
	void on_v1_by_edit_textEdited();
	void on_v2_ax_edit_textEdited();
	void on_v2_ay_edit_textEdited();
	void on_v2_bx_edit_textEdited();
	void on_v2_by_edit_textEdited();

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();
};
#endif // MAINWINDOW_H
