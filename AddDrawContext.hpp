#ifndef ADDCONTEXT_H
#define ADDCONTEXT_H

#include "IAddDrawStrategy.h"

class AddDrawContext
{
private:
	IAddDrawStrategy* _strategy;
public:
	AddDrawContext(IAddDrawStrategy* strategy) : _strategy(strategy) { }
	void setAddDrawStrategy(IAddDrawStrategy* newStrategy) { _strategy = newStrategy; }
	Vector2& addVectors(const Vector2& v1, const Vector2& v2, QPainter* painter)
	{
		return _strategy->add(v1, v2, painter);
	}
};

#endif // ADDCONTEXT_H
