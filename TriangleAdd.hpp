#ifndef TRIANGLEADD_H
#define TRIANGLEADD_H

#include "IAddDrawStrategy.h"

class TriangleAdd : public IAddDrawStrategy
{
public:
	Vector2& add(const Vector2 &v1, const Vector2 &v2, QPainter *painter) override
	{
		// Переносим 2 вектор к концу 1-го.
		IntPtr<Vector2> temp(new Vector2(v1.bx, v1.by, v1.bx + v2.bx - v2.ax, v1.by + v2.by - v2.ay));
		PaintHelper::drawVector(*temp, "temp (v2)", painter);

		// Получаем результат сложения векторов.
		IntPtr<Vector2> res(new Vector2(v1.ax, v1.ay, temp->bx, temp->by));
		QPen p = painter->pen();
		painter->setPen(QPen(Qt::red, 3, Qt::SolidLine, Qt::FlatCap));
		PaintHelper::drawVector(*res, "res", painter);
		painter->setPen(p);

		return *res;
	}
};

#endif // TRIANGLEADD_H
