#ifndef PAINTHELPER_H
#define PAINTHELPER_H

#include <QPainter>
#include "Vector2.hpp"

class PaintHelper
{
public:
	static void drawVector(const Vector2& v, const QString& text, QPainter* painter)
	{
		painter->drawLine(v.ax, v.ay, v.bx, v.by);
		painter->drawText((v.ax + v.bx) / 2, (v.ay + v.by) / 2, text);
	}
};

#endif // PAINTHELPER_H
