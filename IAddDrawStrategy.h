#ifndef IADDSTRATEGY_H
#define IADDSTRATEGY_H

#include <QPainter>
#include "IntPtr.hpp"
#include "Vector2.hpp"
#include "PaintHelper.hpp"

class IAddDrawStrategy
{
public:
	virtual Vector2& add(const Vector2& v1, const Vector2& v2, QPainter* painter) = 0;
};

#endif // IADDSTRATEGY_H
