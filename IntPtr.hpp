#ifndef INTPTR_H
#define INTPTR_H

template<class T>
class IntPtr
{
private:
	T* ptr;
public:
	IntPtr(T* objPtr) : ptr(objPtr) { }
	~IntPtr() { delete ptr; }
	T* operator &() { return ptr; }
	T* operator ->() { return ptr; }
	T& operator *() { return *ptr; }
};

#endif // INTPTR_H
